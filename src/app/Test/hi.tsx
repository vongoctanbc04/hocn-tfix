"use client"
import { useEffect, useState } from "react"

const TestPage = () => {

    const [number, setNumber] = useState(() => {
        const cookieNumber = getCookie("number");
        return cookieNumber ? parseInt(cookieNumber) : 10;
    });
    // const [number, setNumber] = useState(() => {
    //     const localNumber = localStorage.getItem("number")
    //     return localNumber ? parseInt(localNumber) : 10;
    // });
    const [isRunning, setIsRunning] = useState(false);
    const [test, setTest] = useState("Bắt đầu")

    useEffect(() => {
        let intervalId: string | number | NodeJS.Timeout | undefined;

        if (isRunning) {
            intervalId = setInterval(() => {
                setNumber((prevNumber) => {
                    if (prevNumber === 0) {
                        setTest("Kết thúc");
                        clearInterval(intervalId);
                        setIsRunning(false);
                        return 0;
                    }
                    return prevNumber - 1
                });
            }, 1000);
        }

        return () => {
            clearInterval(intervalId);
        };
    }, [isRunning]);

    const handleClick = () => {
        if (isRunning) {
            setIsRunning(false);
            setTest("Giảm");
        } else {

            setIsRunning(true);
            setTest("Dừng");
            // console.log("tan1")
        }

        if (number === 0) {
            setTest("Bắt đầu");
            setNumber(10);
            setIsRunning(false)
            // console.log("tan3")

        }

    };

    // useEffect(() => {
    //     localStorage.setItem("number", number.toString());
    // }, [number]);

    useEffect(() => {
        setCookie("number", number, 30); // Lưu giá trị number vào cookie với thời gian tồn tại là 30 ngày
    }, [number]);

    // Hàm để lấy giá trị của cookie
    function getCookie(name: string | any[]) {
        const cookies = document.cookie.split(";").map(cookie => cookie.trim());
        for (let cookie of cookies) {
            if (cookie.startsWith(name + "=")) {
                return cookie.substring(name.length + 1);
            }
        }
        return null;
    }

    // Hàm để thiết lập giá trị của cookie
    function setCookie(name: string, value: number, days: number) {
        const expirationDate = new Date();
        expirationDate.setDate(expirationDate.getDate() + days);
        const cookie = `${name}=${value}; expires=${expirationDate.toUTCString()}; path=/`;
        document.cookie = cookie;
    }

    return <div className="pt-32 text-center">
        <h1 className="text-red-300 text-7xl font-bold">{number === 0 ? "Bắt đầu lại" : number}</h1>
        <button className="text-lime-400 bg-black px-8 py-4 text-3xl rounded-full" onClick={handleClick}>{test}</button>
    </div>
}
export default TestPage
"use client"
import { useState } from 'react';
import { useRouter } from 'next/navigation'
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { addLogin } from '../GlobalRedux/Features/counter/login'
import { RootState } from '../GlobalRedux/store';

const LoginPage = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch()
    const UserLogin = useSelector((state: RootState) => state.login.userData)


    const [condition, setCondition] = useState(false)

    const router = useRouter()

    const handleSubmit = (event: { preventDefault: () => void; }) => {
        event.preventDefault();
        const value = { username, password }
        if (username === 'tan' && password === 'tan') {
            dispatch(addLogin(value))
            window.location.href = '/';
    

        } if (password.length <= 3) {
            setCondition(true)
        } else {
            console.log("thông báo lỗi")
        }
        // setUsername("")
        // setPassword("")
    };
    return <div>
        <div className="relative z-1 ">
            <div className="flex justify-between justify-center w-full pt-2 px-[5%] absolute z-10" >
                <div className="text-red-500 text-6xl font-bold">
                    <Link href="/">NETFLIX</Link>
                </div>
            </div>

            <div className="bg-local bg-center bg-no-repeat text-white relative h-screen" style={{
                backgroundImage: 'url("https://assets.nflxext.com/ffe/siteui/vlv3/9d3533b2-0e2b-40b2-95e0-ecd7979cc88b/5fbcc285-a371-48ce-965b-49abcc9a8f39/VN-vi-20240311-popsignuptwoweeks-perspective_alpha_website_large.jpg")',
                // height: '900px',
                // position: 'relative',
                // zIndex: 1,
            }}>
                <div className="absolute inset-0 bg-black bg-opacity-40"></div>
                <div className="w-[30%] mx-auto relative z-10 pt-[7%]">
                    <div className=' bg-black bg-opacity-70'>
                        <form className="w-[80%] m-auto" onSubmit={handleSubmit}>
                            <div className='text-6xl font-bold pt-10 pb-4'>Đăng nhập</div>
                            <div className='pt-6'>
                                <input
                                    type="text"
                                    id="username"
                                    value={username}
                                    placeholder="Email hoặc số điện thoại"
                                    className="bg-black bg-opacity-70 w-full m-auto h-16 pl-[12px] rounded-md"
                                    onChange={(event) => setUsername(event.target.value)}
                                />
                                <div className="pt-1 text-red-500">Vui lòng nhập email hoặc số điện thoại hợp lệ (phải có @).</div>
                            </div>
                            <div className="pt-6">
                                <input
                                    type="password"
                                    id="password"
                                    placeholder="Mật khẩu"
                                    className="bg-black bg-opacity-70 w-full m-auto h-16 pl-[12px] rounded-md"
                                    value={password}
                                    onChange={(event) => setPassword(event.target.value)}
                                />
                                {condition && <div className="pt-1 text-red-500">Vui lòng password hợp lệ (tối thiểu 4 kí tự).</div>}
                            </div>
                            <button type="submit" className="bg-red-500 w-full h-10 mb-4 mt-6 rounded-md">Đăng nhập</button>
                            <div className="text-center">HOẶC</div>
                            <button className="bg-black bg-opacity-300 w-full h-10 mb-6 mt-2 rounded-md hover:scale-105 " style={{ transition: 'all 0.3s' }}>
                                Sử dụng mã đăng nhập
                            </button>
                            <input type="checkbox" checked /> Ghi nhớ tôi
                            <div className='flex pt-2 text-lg'>
                                <div className=''>Bạn mới tham gia?</div>
                                <button className='text-red-600' >Đăng kí ngay.</button>
                            </div>
                            {/* {isModalOpen && (
                                <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-black bg-opacity-50">
                                    <div className="bg-white p-4 rounded-lg">
                                        {/* Thêm nội dung của bảng thông tin ở đây */}
                            {/* <div className='text-red-600'>Thông tin chi tiết</div> */}
                            {/* <div className='text-red-600'>Thông tin chi tiết</div> */}
                            {/* <div className='text-red-600'>Thông tin chi tiết</div> */}
                            {/* <button onClick={handleCloseModal} className='text-red-600'>Đóng</button> */}
                            {/* </div> */}
                            {/* </div> */}
                            {/* )} */}
                            <div className='pb-6 pt-2 text-xs text-gray-50'>Trang này được Google reCAPTCHA bảo vệ để đảm bảo bạn không phải là robot. <button className='text-sky-500'>Tìm hiểu thêm.</button></div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
}
export default LoginPage
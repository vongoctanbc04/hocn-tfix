"use client";

import { configureStore } from "@reduxjs/toolkit";
import { counterReducer } from "./Features/counter/counterSlice";
import { loginReducer } from "./Features/counter/login";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    login: loginReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

"use client";

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  username: "",
  password: "",
};

// interface data {
//   username: any;
//   password: any;
// }

export const loginSlice = createSlice({
  name: "login",
  initialState: {
    userData: {
      username: "",
      password: "",
    },
  },
  reducers: {
    addLogin: (state, action) => {
      state.userData = action.payload;
    },
  },
});

export const { addLogin } = loginSlice.actions;

export const loginReducer = loginSlice.reducer;

"use client"
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import HomePgae from "./Home/page";
import { RootState } from "./GlobalRedux/store";
import LoginPage from "./Login/page";

export default function Home() {
  const email = useSelector((state: RootState) => state.login.userData);
  console.log("Như", email);

  const [userlocoo, setUserlocoo] = useState(() => {
    return localStorage.getItem("user") || email.username;
  });

  useEffect(() => {
    localStorage.setItem("user", email.username);
  }, [email.username]);

  console.log("Quan", userlocoo);

  return (
    <>
      {userlocoo ? <HomePgae /> : <LoginPage />}

    </>
  );
}
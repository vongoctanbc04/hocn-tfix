
'use client';


import type { RootState } from '../GlobalRedux/store';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement, incrementByAmount } from '../GlobalRedux/Features/counter/counterSlice';
import React, { useRef, useState } from 'react';

const Tan = (prop: any) => {

    const count = useSelector((state: RootState) => state.counter.value);
    const dispatch = useDispatch();

    const countRef = useRef(0);
    const [countt, setCountt] = useState(0);
    console.log("tan", prop)

    const handleClick = () => {
        countRef.current += 1;
        setCountt(countRef.current);
        // console.log("tan", countRef)
    };
    return <>
        <button
            className="p-3 bg-red-300"
            onClick={() => dispatch(increment())}
        >Tăng lên 1</button>
        <span>{count}</span>
        <button
            className="p-3 bg-red-300"
            onClick={() => dispatch(decrement())}
        >Giảm đi 1</button>
        <button
            className="p-3 bg-red-300"
            onClick={() => dispatch(incrementByAmount(2))}
        >Tăng lên  2</button>
        <p>Count: {countt}</p>
        <button onClick={handleClick}>Increase Count</button>
        <p>tan: {countt}</p>

    </>
}
export default Tan
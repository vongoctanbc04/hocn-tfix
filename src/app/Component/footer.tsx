const Footer = () => {
    return <div className="bg-black">
        <div className="max-w-screen-xl m-auto text-slate-500 pt-6">
            <div className="w-[80%] pb-6 pl-[3%]">
                <button className="underline"> Bạn có câu hỏi? Liên hệ với chúng tôi.</button>
                <div className="grid grid-cols-4 gap-16 pt-4 pb-6">
                    <div className=" text-slate-500 underline">
                        <button className="underline">Câu hỏi thường gặp.</button>
                        <button className="underline">Quan hệ với nhà đầu tư</button>
                        <button className="underline">Quyền riêng tư</button>
                        <button className="underline">Kiểm tra tốc độ</button>
                    </div>
                    <div className=" text-slate-500 underline">
                        <button className="underline">Trung tâm trợ giúp</button>
                        <button className="underline">Việc làm</button>
                        <button className="underline">Tùy chọn cookie</button>
                        <button className="underline">Thông báo pháp lý</button>
                    </div>
                    <div className=" text-slate-500 underline">
                        <button className="underline">Tài khoản</button>
                        <button className="underline">Các cách xem</button>
                        <button className="underline">Thông tin doanh nghiệp</button>
                        <button className="underline">Chỉ có trên Netflix</button>
                    </div>
                    <div className=" text-slate-500 underline">
                        <button className="underline">Trung tâm đa phương tiện</button>
                        <button className="underline">Điều khoản sử dụng</button>
                        <button className="underline">Liên hệ với chúng tôi</button>
                        <button className="underline">Netflix Việt Nam</button>
                    </div>
                </div>
                <select className="block w-[13%] mt-6 pl-4 h-10 rounded border border-slate-600 bg-transparent text-white">
                    <option>Tiếng Việt</option>
                    <option>English</option>
                </select>
            </div>
        </div>
    </div>
}
export default Footer
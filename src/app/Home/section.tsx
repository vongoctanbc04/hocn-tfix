/* eslint-disable react/jsx-key */
/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { ChevronLeft, ChevronRight } from 'react-feather';
import Carousel from '../Component/carousel';


const SlickSlider = () => {
    const slides = ["https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png", "https://songnhat.com.vn/wp-content/uploads/2024/01/BANNER-MOTOR2.png",];
    return <div className="flex justify-center">
        <div className="relative">
            <div className="max-w-screen">
                <Carousel slides={slides} />
            </div>
        </div>
    </div>
};

export default SlickSlider;
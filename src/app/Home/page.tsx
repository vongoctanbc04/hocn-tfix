"use client"
import Link from "next/link";
import { useEffect, useState } from "react";
import SlickSlider from "./section";
import Image from "next/image";
import maycnc from "../Image/bacgroudlogo.jpeg"
import ShopOnline from "../Component/\bshopOnline";
import icon1 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 14.48.56.png"
import icon2 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 14.49.06.png"
import icon3 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 14.49.12.png"
import icon4 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 14.49.19.png"
import icon5 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 19.55.31.png"
import icon6 from "../Image/Ảnh chụp Màn hình 2024-03-18 lúc 19.55.54.png"
import { useTranslation } from 'react-i18next';

const HomePgae = () => {
    const { t } = useTranslation();

    const [note, setNote] = useState(false)
    const [note1, setNote1] = useState(false)
    const [note2, setNote2] = useState(false)
    const [note3, setNote3] = useState(false)
    const [note4, setNote4] = useState(false)
    const [note5, setNote5] = useState(false)

    const handleNoteClick = () => {
        setNote(!note);
        setNote1(false);
        setNote2(false);
        setNote3(false);
        setNote4(false);
        setNote5(false);
    };

    const handleNote1Click = () => {
        setNote(false);
        setNote1(!note1);
        setNote2(false);
        setNote3(false);
        setNote4(false);
        setNote5(false);
    };

    const handleNote2Click = () => {
        setNote(false);
        setNote1(false);
        setNote2(!note2);
        setNote3(false);
        setNote4(false);
        setNote5(false);
    };

    const handleNote3Click = () => {
        setNote(false);
        setNote1(false);
        setNote2(false);
        setNote3(!note3);
        setNote4(false);
        setNote5(false);
    };

    const handleNote4Click = () => {
        setNote(false);
        setNote1(false);
        setNote2(false);
        setNote3(false);
        setNote4(!note4);
        setNote5(false);
    };

    const handleNote5Click = () => {
        setNote(false);
        setNote1(false);
        setNote2(false);
        setNote3(false);
        setNote5(!note5);
        setNote4(false);
    };

    return (<div>

        <div className="relative z-1 ">
            <div className="flex justify-between justify-center w-full pt-2 px-[5%] absolute z-10" >
                <div className="text-red-500 text-6xl font-bold">NETFLIX</div>
                <div className="flex items-center">
                    <div className="pr-3">
                        <select className="block w-full h-10 rounded border border-slate-600 bg-transparent text-white">
                            <option>Tiếng Việt</option>
                            <option>English</option>
                        </select>
                    </div>
                    <button className="bg-red-600 hover:bg-red-800 text-white font-bold px-6 rounded h-10">
                        <Link href="/Login">Sign In</Link>
                    </button>
                    <div><Link href="/Test">Test</Link></div>

                </div>
            </div>

            <div className="bg-local bg-center bg-no-repeat text-white relative h-screen" style={{
                backgroundImage: 'url("https://assets.nflxext.com/ffe/siteui/vlv3/9d3533b2-0e2b-40b2-95e0-ecd7979cc88b/5fbcc285-a371-48ce-965b-49abcc9a8f39/VN-vi-20240311-popsignuptwoweeks-perspective_alpha_website_large.jpg")',
                // height: '900px',
                // position: 'relative',
                // zIndex: 1,
            }}>
                <div className="absolute inset-0 bg-black bg-opacity-70"></div>
                <div className="w-[40%] mx-auto relative z-10">
                    <div className="text-7xl font-bold leading-tight text-center pt-52">Chương trình truyền hình, phim không giới hạn và nhiều nội dung khác</div>
                    <div className="text-2xl font-medium text-center pt-6">Giá từ 70.000 ₫. Hủy bất kỳ lúc nào.</div>
                    <div className="pt-4">Bạn đã sẵn sàng xem chưa? Nhập email để tạo hoặc kích hoạt lại tư cách thành viên của bạn.</div>
                    <div className="h-10 mt-6">
                        <input type="text" id="search" className="border border-slate-300" name="search" placeholder="Địa chỉ emai" style={{ width: "78%", height: "100%", paddingLeft: "10px", backgroundColor: 'dimgrey' }}></input>
                        <button className=" ml-4 px-4 bg-red-600 hover:bg-red-800 rounded-md h-full">Bắt đầu</button>
                    </div>
                </div>

            </div>
        </div>

        {/* Màu */}

        <div className="w-full] h-1" style={{ background: "linear-gradient(to right, rgba(33, 13, 22, 1) 16%, rgba(184, 40, 105, 1), rgba(229, 9, 20, 1), rgba(184, 40, 105, 1), rgba(33, 13, 22, 1) 84%);" }}></div>

        {/* CÁC GÓI DỊCH VỤ */}

        <div className="bg-black h-full">

            <div className="max-w-screen-xl m-auto text-white">
                <div className="text-4xl font-sans font-semibold pt-20">Gói dịch vụ đáp ứng nhu cầu của bạn</div>
                <div className="pt-6 flex grid gap-4 grid-cols-4">
                    <div className="bg-gradient-to-t from-rose-800 to-rose-950 rounded-lg hover:scale-105 " style={{ transition: 'all 0.3s' }}>
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-black pt-8 pb-4">CAO CẤP</div>
                            <div className="pb-4 h-[120px]">Trải nghiệm điện ảnh với chất lượng hình ảnh và âm thanh tốt nhất.</div>
                            <div className="pb-6">260.000 ₫/tháng</div>
                        </div>

                    </div>
                    <div className="bg-gradient-to-t from-rose-800 to-rose-950 rounded-lg hover:bg-red-600 hover:scale-105 " style={{ transition: 'all 0.3s' }}>
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-black pt-8 pb-4">TIÊU CHUẨN</div>
                            <div className="pb-4 h-[120px]">Tất cả nội dung giải trí bạn yêu thích, ở chất lượng video Full HD.</div>
                            <div className="pb-6">220.000 ₫/tháng</div>
                        </div>

                    </div>
                    <div className="bg-gradient-to-t from-rose-800 to-rose-950 rounded-lg hover:scale-105 " style={{ transition: 'all 0.3s' }}>
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-black pt-8 pb-4">CƠ BẢN</div>
                            <div className="pb-4 h-[120px]">Cách tuyệt vời để thưởng thức tất cả các tác phẩm yêu thích của bạn với mức giá tiết kiệm.</div>
                            <div className="pb-6">108.000 ₫/tháng</div>
                        </div>

                    </div>
                    <div className="bg-gradient-to-t from-rose-800 to-rose-950 rounded-lg hover:scale-105 " style={{ transition: 'all 0.3s' }}>
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-black pt-8 pb-4">DI ĐỘNG</div>
                            <div className="pb-4 h-[120px]">Lựa chọn phù hợp với việc di chuyển, cho thiết bị di động của bạn.</div>
                            <div className="pb-6">70.000 ₫/tháng</div>
                        </div>

                    </div>
                </div>
            </div>

            {/* Thêm lý do để tham gia */}

            <div className="max-w-screen-xl m-auto text-white">
                <div className="text-4xl font-sans font-semibold pt-14">Thêm lý do để tham gia</div>
                <div className="pt-6 flex grid gap-4 grid-cols-4">
                    <div className="bg-gradient-to-t from-indigo-700 to-indigo-950 rounded-lg ">
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-medium pt-8 pb-4 h-[160px]">Thưởng thức trên TV của bạn</div>
                            <div className="pb-4 h-[170px] leading-normal">Xem trên TV thông minh, Playstation, Xbox, Chromecast, Apple TV, đầu phát Blu-ray và nhiều thiết bị khác.</div>
                            <div className="pb-6 flex justify-end">
                                <Image src={icon1} alt="icon" />
                            </div>
                        </div>
                    </div>
                    <div className="bg-gradient-to-t from-indigo-700 to-indigo-950 rounded-lg ">
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-medium pt-8 pb-4 h-[160px]">Tải xuống nội dung để xem ngoại tuyến</div>
                            <div className="pb-4 h-[170px] leading-normal">Lưu lại những nội dung yêu thích một cách dễ dàng và luôn có thứ để xem.</div>
                            <div className="pb-6 flex justify-end">
                                <Image src={icon2} alt="icon" />
                            </div>
                        </div>
                    </div>
                    <div className="bg-gradient-to-t from-indigo-700 to-indigo-950 rounded-lg ">
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-medium pt-8 pb-4 h-[160px]">Tạo hồ sơ cho trẻ em.</div>
                            <div className="pb-4 h-[170px] leading-normal">Phát trực tuyến không giới hạn phim và chương trình truyền hình trên điện thoại, máy tính bảng, máy tính xách tay và TV.</div>
                            <div className="pb-6 flex justify-end">
                                <Image src={icon3} alt="icon" />
                            </div>
                        </div>
                    </div>
                    <div className="bg-gradient-to-t from-indigo-700 to-indigo-950 rounded-lg ">
                        <div className="w-[70%] m-auto">
                            <div className="text-3xl font-medium pt-8 pb-4 h-[160px]">Thưởng thức trên TV của bạn</div>
                            <div className="pb-4 h-[170px] leading-normal">Đưa các em vào những cuộc phiêu lưu với nhân vật được yêu thích trong một không gian riêng. Tính năng này đi kèm miễn phí với tư cách thành viên của bạn.</div>
                            <div className="pb-6 flex justify-end">
                                <Image src={icon4} alt="icon" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            c

            <div className="max-w-screen-xl m-auto text-white">
                <div className="text-4xl font-sans font-semibold pt-20">Câu hỏi thường gặp</div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNoteClick}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Netflix là gì?</div>
                            {note ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Netflix là dịch vụ phát trực tuyến mang đến đa dạng các loại chương trình truyền hình, phim, anime, phim tài liệu đoạt giải thưởng và nhiều nội dung khác trên hàng nghìn thiết bị có kết nối Internet.</div>
                            <div className="pt-4 w-[92%] m-auto pb-4">Bạn có thể xem bao nhiêu tùy thích, bất cứ lúc nào bạn muốn mà không gặp phải một quảng cáo nào – tất cả chỉ với một mức giá thấp hàng tháng. Luôn có những nội dung mới để bạn khám phá và những chương trình truyền hình, phim mới được bổ sung mỗi tuần!</div>
                        </div>
                    )}
                </div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNote1Click}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Tôi phải trả bao nhiêu tiền để xem Netflix?</div>
                            {note1 ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note1 && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Xem Netflix trên điện thoại thông minh, máy tính bảng, TV thông minh, máy tính xách tay hoặc thiết bị phát trực tuyến, chỉ với một khoản phí cố định hàng tháng. Các gói dịch vụ với mức giá từ 260.000 ₫ đến 70.000 ₫ mỗi tháng. Không phụ phí, không hợp đồng..</div>
                            <div className="pt-4 w-[92%] m-auto pb-4"></div>
                        </div>
                    )}
                </div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNote2Click}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Tôi có thể xem ở đâu?</div>
                            {note2 ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note2 && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Xem mọi lúc, mọi nơi. Đăng nhập bằng tài khoản Netflix của bạn để xem ngay trên trang web netflix.com từ máy tính cá nhân, hoặc trên bất kỳ thiết bị nào có kết nối Internet và có cài đặt ứng dụng Netflix, bao gồm TV thông minh, điện thoại thông minh, máy tính bảng, thiết bị phát đa phương tiện trực tuyến và máy chơi game.</div>
                            <div className="pt-4 w-[92%] m-auto pb-4">Bạn cũng có thể tải xuống các chương trình yêu thích bằng ứng dụng trên iOS, Android hoặc Windows 10. Vào phần nội dung đã tải xuống để xem trong khi di chuyển và khi không có kết nối Internet. Mang Netflix theo bạn đến mọi nơi.</div>
                        </div>
                    )}
                </div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNote3Click}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Làm thế nào để hủy?</div>
                            {note3 ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note3 && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Netflix rất linh hoạt. Không có hợp đồng phiền toái, không ràng buộc. Bạn có thể dễ dàng hủy tài khoản trực tuyến chỉ trong hai cú nhấp chuột. Không mất phí hủy – bạn có thể bắt đầu hoặc ngừng tài khoản bất cứ lúc nào.</div>
                            <div className="pt-4 w-[92%] m-auto pb-4"></div>
                        </div>
                    )}
                </div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNote4Click}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Tôi có thể xem gì trên Netflix?</div>
                            {note4 ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note4 && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Netflix có một thư viện phong phú gồm các phim truyện, phim tài liệu, chương trình truyền hình, anime, tác phẩm giành giải thưởng của Netflix và nhiều nội dung khác. Xem không giới hạn bất cứ lúc nào bạn muốn.</div>
                            <div className="pt-4 w-[92%] m-auto pb-4"></div>
                        </div>
                    )}
                </div>
                <div>
                    <div className="w-[80%] bg-stone-800 hover:bg-stone-600 h-16 mt-6" onClick={handleNote5Click}>
                        <button className="flex items-center justify-between w-full h-full">
                            <div className="pl-[4%] text-xl font-semibold">Netflix có phù hợp cho trẻ em không?</div>
                            {note5 ? (<Image src={icon5} alt="hihi" className="mr-[2%]" />) : (<Image src={icon6} alt="hihi" className="mr-[2%]" />)}
                        </button>
                    </div>
                    {note5 && (
                        <div className="bg-stone-800 w-[80%] mt-1">
                            <div className="pt-8 w-[92%] m-auto">Trải nghiệm Netflix Trẻ em có sẵn trong gói dịch vụ của bạn, trao cho phụ huynh quyền kiểm soát trong khi các em có thể thưởng thức các bộ phim và chương trình phù hợp cho gia đình tại không gian riêng.</div>
                            <div className="pt-4 w-[92%] m-auto pb-4">Hồ sơ Trẻ em đi kèm tính năng kiểm soát của cha mẹ (được bảo vệ bằng mã PIN), cho phép bạn giới hạn độ tuổi cho nội dung con mình được phép xem, cũng như chặn những phim hoặc chương trình mà bạn không muốn các em nhìn thấy.</div>
                        </div>
                    )}
                </div>
            </div>

            {/* END */}

            <div className="max-w-screen-xl m-auto text-white pt-24">Bạn đã sẵn sàng xem chưa? Nhập email để tạo hoặc kích hoạt lại tư cách thành viên của bạn.</div>
            <div className="max-w-screen-xl m-auto text-white pt-6">
                <div className="h-10 mt-6">
                    <input type="text" id="search" className="rounded-md border border-slate-300" name="search" placeholder="Địa chỉ emai" style={{ width: "78%", height: "100%", paddingLeft: "10px", backgroundColor: 'dimgrey' }}></input>
                    <button className=" ml-4 px-4 bg-red-600 hover:bg-red-800 rounded-md h-full">Bắt đầu</button>
                </div>
            </div>


        </div>

    </div>


    )
}
export default HomePgae
